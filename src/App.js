import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import { Header } from './components/index.js';
import { Home, Cart } from './pages/index.js';

import './App.css';

function App() {

  return (
    <div className='wrapper'>
      <Header />
      <div className='content'>
        <Switch>
          <Route path='/' component={Home} exact />
          <Route path='/cart' component={Cart} exact />
          <Redirect to='/' />
        </Switch>
      </div>
    </div>
  );
}

export default App;
