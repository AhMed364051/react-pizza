import React from 'react';
import classNames from 'classnames';

const Button = ({ onClick, outline, className, children }) => {
  return (
    <a
      href='/cart.html'
      onClick={onClick}
      className={classNames('button', className, {
        'button--outline': outline,
      })}
    >
      {children}
    </a>
  );
};

export default Button;
