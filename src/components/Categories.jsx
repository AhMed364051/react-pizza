import React from 'react';
import PropTypes from 'prop-types';

const Categories = React.memo(({ items, onClickCategory, activeCategory }) => {
  return (
    <div className='categories'>
      <ul>
        <li className={activeCategory === null ? 'active' : ''} onClick={() => onClickCategory(null)}>
          Все
        </li>
        {items &&
          items.map((name, index) => (
            <li
              key={`${name}_${index}`}
              className={activeCategory === index ? 'active' : ''}
              onClick={() => onClickCategory(index)}
            >
              {name}
            </li>
          ))}
      </ul>
    </div>
  );
});

Categories.defaultProps = {
  items: [],
};

Categories.propTypes = {
  types: PropTypes.arrayOf(PropTypes.string),
};

export default Categories;
