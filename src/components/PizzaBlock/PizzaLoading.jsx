import React from 'react';
import ContentLoader from 'react-content-loader';

const PizzaLoading = () => (
  <ContentLoader
    className='pizza__block'
    speed={2}
    width={280}
    height={465}
    viewBox='0 0 280 465'
    backgroundColor='#f3f3f3'
    foregroundColor='#ecebeb'
  >
    <rect x='118' y='32' rx='0' ry='0' width='1' height='0' />
    <circle cx='136' cy='125' r='122' />
    <rect x='15' y='259' rx='3' ry='3' width='250' height='30' />
    <rect x='15' y='307' rx='3' ry='3' width='250' height='76' />
    <rect x='17' y='410' rx='3' ry='3' width='92' height='27' />
    <rect x='126' y='404' rx='20' ry='20' width='140' height='40' />
  </ContentLoader>
);

export default PizzaLoading;
