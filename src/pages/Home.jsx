import React from 'react';
import { useSelector, useDispatch } from 'react-redux';

import { fetchPizzas } from '../redux/actions/pizzas';
import { setCategory, setSortBy } from '../redux/actions/filters';

import { Categories, Sort, PizzaBlock, PizzaLoading } from '../components';

const categoryNames = ['Мясные', 'Вегетарианские', 'Гриль', 'Острые', 'Закрытые'];

const sortItems = [
  { name: 'популярности', type: 'rating' },
  { name: 'цене', type: 'price' },
  { name: 'алфавиту', type: 'name' },
];

const Home = () => {
  const dispatch = useDispatch();
  const items = useSelector(({ pizzas }) => pizzas.items);
  const isLoaded = useSelector(({ pizzas }) => pizzas.isLoaded);
  const {category, sortBy} = useSelector(({filters}) => filters);


  React.useEffect(() => {
    dispatch(fetchPizzas(category, sortBy));
  }, [category, sortBy]);

  const onClickCategory = React.useCallback((index) => {
    dispatch(setCategory(index));
  }, []);

  const onClickSortBy = React.useCallback((name) => {
    dispatch(setSortBy(name));
  }, []);

  return (
    <div className='container'>
      <div className='content__top'>
        <Categories items={categoryNames} onClickCategory={onClickCategory} activeCategory={category} />
        <Sort items={sortItems} onClickSortBy={onClickSortBy} activeSortType={sortBy} />
      </div>
      <h2 className='content__title'>Все пиццы</h2>
      <div className='content__items'>
        {
          isLoaded ? items.map((obj) => (
            <PizzaBlock key={obj.id} {...obj} />
          )) : Array(12).fill(0).map((_, index) => <PizzaLoading key={index} />)
        }
      </div>
    </div>
  );
};

export default Home;
